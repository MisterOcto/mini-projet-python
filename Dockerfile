FROM python:3.8.10

RUN apt-get update && \
    apt-get install -y docker.io

WORKDIR /pokedexpy

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . .